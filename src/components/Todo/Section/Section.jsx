import React, {useState} from 'react';
import {Button, Select,Tag } from 'antd';
import './style.css';

const {Option} = Select;

const Section = (props) =>{
    const [status, setStatus] = useState(props.status);

    const resetStatus = (value) =>{
        setStatus((value === 'completed')?true: false);
        props.changeStatus(props.id);
    };

    return(
        <div className = 'actionSection'>
            <div id = 'number' style = {{color: (status)? '#b0acac': '#000000'}}>
                {(props.number+1) + '.'}
            </div>

            <div id='name' style = {(status)? {textDecoration:  'line-through', color: '#b0acac'} : {textDecoration: 'none', color: '#000000'}}>
                {props.name}
            </div>

            <div id='status'>
                <Select className = 'select' value = {(props.status)? 'completed': 'noCompleted'} onChange = {resetStatus}>
                    <Option value= 'noCompleted'>Not Completed</Option>
                    <Option value= 'completed'>Completed</Option>
                </Select>
            </div>
            <div id='actions'>
                <Button type="danger" onClick = {() => props.removeTask(props.id)}>Delete</Button>
            </div>
            <div id='actions'>
                {(status) ? <Tag color="green">Done</Tag> : <Tag color="orange">Pending</Tag>}
            </div>
        </div>
    );
};

export default Section;